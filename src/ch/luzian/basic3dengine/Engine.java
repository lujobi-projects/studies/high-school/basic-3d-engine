/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine;

import ch.luzian.basic3dengine.graphics.Screen;
import ch.luzian.basic3dengine.input.Keyboard;
import ch.luzian.basic3dengine.input.MouseMovement;
import ch.luzian.basic3dengine.Math.camera.Camera;
import ch.luzian.basic3dengine.Math.coordinatesystem.CoordinateSystem2D;
import ch.luzian.basic3dengine.Math.planeMath.points.Point3D;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector3D;
import ch.luzian.basic3dengine.graphics.queues.RenderQueue;
import ch.luzian.basic3dengine.view.View3D;
import java.awt.Canvas;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Engine extends Canvas implements Runnable, ActionListener {

    private static final String TITLE = "Basic3DEngine - by Luzian Bieri v0.1";
    private static final String PATH = "/res/icon.PNG";
    private static final int WIDTH = 800;
    public  static final double RATIO = (0.5625);
    private static final int HEIGHT = (int) (WIDTH*RATIO);
    private static final int SCALE = 1;

    private Thread gameThread;
    private boolean running;

    private JFrame frame;
    
    private MouseMovement mouse;
    private Keyboard key; 
    private Camera camera;
    private Screen screen;
    private RenderQueue renderQueue;
    private View3D view;

    private BufferedImage image;
    private int[] pixels;
    
    

    public static void main(String[] args) {
        Engine pathfinding = new Engine();
        try {
            pathfinding.frame.setIconImage(ImageIO.read(Engine.class.getResource(PATH)));
        } catch (IOException ex) {
        }
        pathfinding.frame.setResizable(false);
        pathfinding.frame.setTitle(TITLE);
        Container container = pathfinding.frame.getContentPane();

        addComponentsToContainer(container, pathfinding);

        pathfinding.frame.pack();

        pathfinding.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pathfinding.frame.setLocationRelativeTo(null);
        pathfinding.frame.setVisible(true);

        pathfinding.start();

    }

    public Engine() {
        initEngine();
    }

    @Override
    public void run() {
        initControllers();
        long lastTime = System.nanoTime();
        long timer = System.currentTimeMillis();
        final double ns = 1000000000.0 /120.0;
        double delta = 0;
        int frames = 0;
        int ticks = 0;

        requestFocus();

        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;

            while (delta >= 1) {
                tick();
                ticks++;
                delta--;
            }

            render();
            frames++;

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                frame.setTitle(TITLE + " | " + ticks + " tps, " + frames + " fps");
                System.out.println(ticks + " tps, " + frames + " fps");
                frames = 0;
                ticks = 0;
            }
        }
    }

    public synchronized void start() {
        running = true;
        gameThread = new Thread(this, "Pathfinding");
        gameThread.start();
    }

    public synchronized void stop() {
        running = false;
        try {
            gameThread.join();
        } catch (InterruptedException ex) {
        }
    }

    private void tick() {
        key.update();
        mouse.update();    
        camera.update();
        screen.update();
        renderQueue.update();
        view.update();       
    }
    
    private void initControllers() {
        key.init();
        mouse.init();    
        camera.init(); 
        screen.init(); 
        renderQueue.init(); 
        view.init();      
    }
    
    private void render() {
        BufferStrategy bs = getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(3);
            return;
        }

        renderQueue.render();
        
        System.arraycopy(screen.pixels, 0, pixels, 0, pixels.length);

        Graphics g = bs.getDrawGraphics();
        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        g.dispose();
        bs.show();
    }

    private void initEngine() {
        frame = new JFrame();
        
        key = new Keyboard();
        mouse = new MouseMovement(WIDTH/2, HEIGHT/2, frame, this);
        
        camera = new Camera(WIDTH, HEIGHT, Math.PI / 5*3, new Point3D(0, 0, 0), new Vector3D(0, 0, 0), 0, 50, mouse, key);
        screen = new Screen(WIDTH, HEIGHT);
        CoordinateSystem2D coordSys = new CoordinateSystem2D(WIDTH, HEIGHT); 
        renderQueue = new RenderQueue(camera, coordSys, screen);
        view = new View3D(camera.position, renderQueue);

        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

        Dimension size = new Dimension(WIDTH * SCALE, HEIGHT * SCALE);
        this.setSize(size);
        
        addKeyListener(key);
        addMouseMotionListener(mouse);
    }

    private static void addComponentsToContainer(Container container, Engine engine) {
        container.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(10, 10, 10, 10);
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 0;
        container.add(engine, c);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
