/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.graphics;

import ch.luzian.basic3dengine.controlInterfaces.Controller;


/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Screen implements Controller{

    public final int width, height;
    public int[] pixels;
    private int color = 0xFFFFFFFF;

    public Screen(int width, int height) {
        this.width = width;
        this.height = height;
        pixels = new int[width * height];
        clear();
    }

    @Override
    public void init() {
        
    }

    @Override
    public void update() {
        //Camera Update-Code goes here
    }
    
    public void clear() {
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = 0;
        }
        
    }

    public void setColor(int color) {
        this.color = color;
    }

    private void drawPixel(int x, int y) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            pixels[x + (y * width)] = color;
        } else {
        }
    }

    public void drawCircle(int x, int y, int r) {
        for (double a = 0; a < Math.PI * 2; a += 0.002) {
            drawPixel(fromPolarToCartesianX(a, r, x), fromPolarToCartesianY(a, r, y));
        }
    }

    public void fillCircle(int x, int y, int r) {
        int radiusSquare = r*r;
        for (int i = -r-2; i < r + 2; i++) {
            for (int j = -r-2; j < r +2; j++) {
                if(radiusSquare-1>=i*i+(j*j)){
                    drawPixel(i+x, j+y);
                }
            }
        }
    }

    public void drawRectangle(int x, int y, int width, int height) {
        for (int i = 0; i <= width; i++) {
            drawPixel(i + x, y);
            drawPixel(i + x, y + height);
        }
        for (int i = 0; i <= height; i++) {
            drawPixel(x, i + y);
            drawPixel(x + width, i + y);
        }
    }

    public void fillRectangle(int x, int y, int width, int height) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                drawPixel(i + x, j + y);
            }
        }
    }
    
    public void drawLine(int x1, int y1, int x2, int y2){
        double diffX = x2 - x1;
        double diffY = y2 - y1;
        double length = Math.sqrt((diffX *diffX) + (diffY * diffY));
        double stepX = diffX / length /2;
        double stepY = diffY / length /2;
        for(int i = 0; i < length *2; i ++){
            drawPixel((int)(i*stepX + x1), (int) (i*stepY+y1));
        }
        drawPixel(x2, y2);
    }
    
    public void drawTriangle(Triangle triangle){
        for(int i = 0; i < triangle.xTriangleMax; i++){
            for(int j = 0; j < triangle.yTriangleMax; j++){
                if (triangle.lines[i][j]){
                    drawPixel(i + triangle.xOffset, j + triangle.yOffset);
                }
            }
        }
    }
    
    public void fillTriangle(Triangle triangle){
        for(int i = 0; i < triangle.xTriangleMax; i++){
            for(int j = 0; j < triangle.yTriangleMax; j++){
                if (triangle.fill[i][j]){
                    drawPixel(i + triangle.xOffset, j + triangle.yOffset);
                }
            }
        }
    }

    private int fromPolarToCartesianX(double angle, double radius, int xOffset) {
        return (int) Math.round(Math.cos(angle) * radius + xOffset);
    }

    private int fromPolarToCartesianY(double angle, double radius, int yOffset) {
        return (int) Math.round(Math.sin(angle) * radius + yOffset);
    }
}
