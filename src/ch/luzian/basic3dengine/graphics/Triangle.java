/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.graphics;

import ch.luzian.basic3dengine.Math.planeMath.lines.Line;
import java.util.ArrayList;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Triangle {

    public final ArrayList<Integer> verticesX = new ArrayList<>();
    public final ArrayList<Integer> verticesY = new ArrayList<>();
    private final ArrayList<Line> edges = new ArrayList<>();
    public int xTriangleMax, yTriangleMax;
    public int xOffset = Integer.MAX_VALUE, yOffset = Integer.MAX_VALUE;
    public boolean[][] lines;
    public boolean[][] fill;

    public Triangle(int... vertices) {
        if (vertices.length % 2 == 0) {
            for (int i = 0; i < vertices.length; i += 2) {
                if (xTriangleMax < vertices[i]) {
                    xTriangleMax = vertices[i];
                }
                if (xOffset > vertices[i]) {
                    xOffset = vertices[i];
                }
                if (yTriangleMax < vertices[i + 1]) {
                    yTriangleMax = vertices[i + 1];
                }
                if (yOffset > vertices[i + 1]) {
                    yOffset = vertices[i + 1];
                }
            }

            xTriangleMax -= xOffset - 4;
            yTriangleMax -= yOffset - 4;

            lines = new boolean[xTriangleMax][yTriangleMax];
            fill = new boolean[xTriangleMax][yTriangleMax];

            for (int i = 0; i < vertices.length; i += 2) {
                verticesX.add(vertices[i] - xOffset);
                verticesY.add(vertices[i + 1] - yOffset);
            }
            for (int i = 0; i < verticesX.size() - 1; i++) {
                edges.add(new Line(verticesX.get(i), verticesY.get(i), verticesX.get(i + 1), verticesY.get(i + 1)));
            }
            edges.add(new Line(verticesX.get(verticesX.size() - 1), verticesY.get(verticesX.size() - 1), verticesX.get(0), verticesY.get(0)));
            drawLines();
            fill();
            xOffset += 2;
            yOffset += 2;
        } else {
            System.err.println("not a Polygon");
        }
    }

    private void drawLines() {
        for (Line edge : edges) {
            for (int x = 0; x < xTriangleMax; x++) {
                int bestY = edge.findBestYtoX(x);
                if (bestY != -1) {
                    lines[x + 2][bestY + 2] = true;
                }
            }
        }
        for (Line edge : edges) {
            for (int y = 0; y < yTriangleMax; y++) {
                int bestX = edge.findBestXtoY(y);
                if (bestX != -1) {
                    lines[bestX + 2][y + 2] = true;
                }
            }
        }
    }

    private void fill() {
        for (int j = 0; j < xTriangleMax; j++) {
            boolean actual = false;
            int changesCount = 0;
            for (int i = 0; i < yTriangleMax; i++) {
                if (lines[j][i] != actual) {
                    changesCount++;
                    actual = !actual;
                }
            }

            if (changesCount > 3) {
                if (changesCount % 4 == 0) {
                    boolean draw = false;
                    for (int i = 1; i < yTriangleMax - 1; i++) {
                        if (draw == false && lines[j][i] == true && lines[j][i + 1] == false) {
                            draw = true;
                        } else if (draw == true && lines[j][i] == true && lines[j][i + 1] == false) {
                            draw = false;
                        }
                        fill[j][i] = draw;
                    }
                } else {
                    //!!!!needed for complicated shit
                }
            }
        }
    }
}
