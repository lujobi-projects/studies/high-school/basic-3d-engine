/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.graphics.figures;

import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector3D;
import ch.luzian.basic3dengine.graphics.figures.interfaces.SimpleFigure;
import ch.luzian.basic3dengine.Math.planeMath.lines.Edge;
import ch.luzian.basic3dengine.Math.planeMath.planes.PlaneTriangle;
import ch.luzian.basic3dengine.Math.planeMath.points.Point2D;
import ch.luzian.basic3dengine.Math.planeMath.points.Point3D;
import java.util.ArrayList;
/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public abstract class Figure3D implements SimpleFigure{

    private final ArrayList<PlaneTriangle> triangles = new ArrayList<>();
    private final ArrayList<Edge> edges = new ArrayList<>();
    private final ArrayList<Point3D> vertices = new ArrayList<>();
    
    protected Point3D anchor, cameraPosition;
       
    private Vector3D movement = new Vector3D(0, 0, 0);
    private Vector3D rotation = new Vector3D(0, 0, 0);
    private Point2D anchor2D;

    public Figure3D(Point3D anchor, Point3D cameraPosition) {
        this.anchor = anchor;
        this.cameraPosition = cameraPosition;
    }

    protected void addTriangle(Point3D p1, Point3D p2, Point3D p3) {
        Point3D point1 = getFittingPoint(p1);
        Point3D point2 = getFittingPoint(p2);
        Point3D point3 = getFittingPoint(p3);
        
        Edge edge1 = getFittingEdge(new Edge(point2, point3));
        Edge edge2 = getFittingEdge(new Edge(point1, point2));
        Edge edge3 = getFittingEdge(new Edge(point1, point3));
        
        triangles.add(new PlaneTriangle(point1, point2, point3, edge1, edge2, edge3));
    }

    private Point3D getFittingPoint(Point3D p){
        for (Point3D pInList : vertices) {
            if(pInList.equals(p)){
                return pInList;
            }
        }
        vertices.add(p);
        return p;
    }  
    private Edge getFittingEdge(Edge e){
        for (Edge edgeInList: edges ){
            if(edgeInList.equals(e)){
                return edgeInList;
            }
        }
        edges.add(e);
        return e;
    }

    public void update() {
        if (movement.x != 0 || movement.y != 0 || movement.z != 0) {
            for (Point3D p : vertices) {
                p.addVector(movement);
            }
        }
        if (rotation.x != 0 || rotation.y != 0 || rotation.z != 0) {
            rotateAllVertices(rotation);
        }
    }
    
    public void move(Vector3D movementDir) {
        movement = movementDir;
    }
    public void rotate(Vector3D rotation) {
        this.rotation = rotation;
    }

    private void rotateAllVertices(Vector3D rotation) {
        for(Point3D p : vertices){
            p.rotate(anchor, rotation);
        }
    }

    public ArrayList<PlaneTriangle> getTriangles() {
        return triangles;
    }
    public ArrayList<Edge> getEdges() {
        return edges;
    }
    public ArrayList<Point3D> getVertices() {
        return vertices;
    }
    
    
    @Override
    public void generate() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "3DFigure at " + anchor + " containing: " + vertices.size() + " vertices, " + edges.size() + " edges and " + triangles.size() + " triangles ";
    }
    
    
}
