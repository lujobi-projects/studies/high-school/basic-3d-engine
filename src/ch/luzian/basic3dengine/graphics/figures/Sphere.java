/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.graphics.figures;

import ch.luzian.basic3dengine.graphics.figures.interfaces.SimpleFigure;
import ch.luzian.basic3dengine.Math.camera.Camera;
import ch.luzian.basic3dengine.Math.coordinatesystem.CoordinateSystem2D;
import ch.luzian.basic3dengine.Math.planeMath.points.Point3D;
import java.util.ArrayList;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Sphere extends Figure3D implements SimpleFigure {

    private final double radius;
    private final int precision;

    private final ArrayList<ArrayList<Point3D>> strips = new ArrayList<>();

    public Sphere(Point3D anchor, Point3D cameraPosition, double radius, int precision) {
        super(anchor, cameraPosition);
        this.radius = radius;
        this.precision = precision;
    }

    @Override
    public void generate() {
        ArrayList<Point3D> strip;
        ArrayList<Point3D> strip2;
        for (int i = 0; i < precision + 1; i++) {
            double gamma = Math.PI * i / precision;
            strip = new ArrayList<>();
            for (int j = 0; j < precision+1; j++) {
                double theta = Math.PI * 2 * j / precision;
                strip.add(new Point3D((radius * Math.cos(theta) * Math.sin(gamma)) + anchor.x, 
                        (radius * Math.sin(theta) * Math.sin(gamma)) + anchor.y, 
                        (radius * Math.cos(gamma)) + anchor.z, cameraPosition));
            }
            strips.add(strip);
        }
        for (int i = 0; i < strips.size() - 1; i++) {
            strip = strips.get(i);
            strip2 = strips.get(i + 1);
            for (int j = 0; j < strip.size() - 1; j++) {
                super.addTriangle(strip.get(j), strip.get(j + 1), strip2.get(j));
                super.addTriangle(strip2.get(j), strip2.get(j + 1), strip.get(j + 1));
            }
        }
        super.generate();
    }

    @Override
    public String toString() {
        return super.toString() + "Sphere with radius = " + radius + " and precision = " + precision;
    }

}
