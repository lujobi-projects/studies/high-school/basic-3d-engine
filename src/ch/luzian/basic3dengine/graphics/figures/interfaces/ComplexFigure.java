/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.graphics.figures.interfaces;

import ch.luzian.basic3dengine.graphics.Screen;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector3D;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public interface ComplexFigure {
    public void render(Screen screen);
    public void update();
    public void move(Vector3D movementDir);
    public void rotateX(double angle);
    public void rotateY(double angle);
    public void rotateZ(double angle);
    public void rotate(Vector3D rotation);
}
