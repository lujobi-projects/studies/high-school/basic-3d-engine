/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.graphics.queues;

import ch.luzian.basic3dengine.Math.camera.Camera;
import ch.luzian.basic3dengine.Math.coordinatesystem.CoordinateSystem2D;
import ch.luzian.basic3dengine.Math.planeMath.lines.Edge;
import ch.luzian.basic3dengine.Math.planeMath.planes.PlaneTriangle;
import ch.luzian.basic3dengine.Math.planeMath.points.Point2D;
import ch.luzian.basic3dengine.Math.planeMath.points.Point3D;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector2D;
import ch.luzian.basic3dengine.controlInterfaces.Controller;
import ch.luzian.basic3dengine.graphics.Screen;
import ch.luzian.basic3dengine.graphics.Triangle;
import ch.luzian.basic3dengine.graphics.figures.Figure3D;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class RenderQueue implements Controller {

    private final ArrayList<Figure3D> figures = new ArrayList<>();

    private final ArrayList<Point3D> vertices = new ArrayList<>();
    private final ArrayList<Edge> edges = new ArrayList<>();
    private final ArrayList<PlaneTriangle> triangles = new ArrayList<>(); //Attention eventually AnchorProgramming

    private final Screen screen;
    protected final Camera camera;
    private final CoordinateSystem2D coordSys;

    private boolean updated = false;

    private int vertexIndex;
    private int edgeIndex;
    private int triangleIndex;

    private double furthestVertexDistance;
    private double furthestEdgeDistance;
    private double furthestTriangleDistance;

    public RenderQueue(Camera camera, CoordinateSystem2D coordSys, Screen screen) {
        this.camera = camera;
        this.coordSys = coordSys;
        this.screen = screen;
    }

    @Override
    public void init() {

    }

    public void addToRenderQueue(Figure3D figure) {
        figures.add(figure);
        vertices.addAll(figure.getVertices());
        edges.addAll(figure.getEdges());
        triangles.addAll(figure.getTriangles());
    }

    public void render() {
        screen.clear();
        if (updated) {
            vertexIndex = 0;
            edgeIndex = 0;
            triangleIndex = 0;

            furthestVertexDistance = vertices.get(vertexIndex).getDistanceToTarget();
            furthestEdgeDistance = edges.get(edgeIndex).getDistanceToTarget();
            furthestTriangleDistance = triangles.get(triangleIndex).getDistanceToTarget();
            while (vertexIndex < vertices.size() || edgeIndex < edges.size() || triangleIndex < triangles.size()) {

                if (furthestTriangleDistance >= furthestEdgeDistance && furthestTriangleDistance >= furthestVertexDistance) {
                    renderNextTriangle();
                } else if (furthestEdgeDistance >= furthestTriangleDistance && furthestEdgeDistance >= furthestVertexDistance) {
                    renderNextEdge();
                } else if (furthestVertexDistance >= furthestEdgeDistance && furthestVertexDistance >= furthestTriangleDistance) {
                    renderNextVertex();
                }
            }
        }
    }

    @Override
    public void update() {
        updated = true;
        for (Figure3D f : figures) {
            f.update();
        }
        for (Point3D p : vertices) {
            p.update();
        }
        for (Edge e : edges) {
            e.update();
        }
        for (PlaneTriangle t : triangles) {
            t.update();
        }

        calculateVectors();

        Collections.sort(vertices);
        Collections.sort(edges);
        Collections.sort(triangles);
    }

    private void calculateVectors() {
        for (Point3D p : vertices) {
            p.projectionToCameraPlane = adjust2DVectorToCoordinateSystem(get2DVectorFromCamera(p));
        }
    }

    private Vector2D get2DVectorFromCamera(Point3D point3D) {
        return camera.inputVector(point3D);
    }

    private Point2D adjust2DVectorToCoordinateSystem(Vector2D vector2D) {
        if (vector2D == null) {
            return null;
        }
        int x = coordSys.getPixelX(vector2D.x);
        int y = coordSys.getPixelY(vector2D.y);
        return new Point2D(x, y);
    }

    private void renderNextVertex() {
        Point3D point = vertices.get(vertexIndex);

        Point2D p1 = point.projectionToCameraPlane;

        if (p1 != null) {
            screen.setColor(point.color.getRGB());
            screen.fillCircle((int) p1.x, (int) p1.y, 4);
        }

        vertexIndex++;
        if (vertexIndex < vertices.size()) {
            furthestVertexDistance = vertices.get(vertexIndex).getDistanceToTarget();
        } else {
            furthestVertexDistance = -Double.MAX_VALUE;
        }

    }

    private void renderNextEdge() {
        Edge edge = edges.get(edgeIndex);

        Point2D p1 = edge.getP1().projectionToCameraPlane;
        Point2D p2 = edge.getP2().projectionToCameraPlane;

        if (p1 != null && p2 != null) {
            screen.setColor(edge.color.getRGB());
            screen.drawLine((int) p1.x, (int) p1.y, (int) p2.x, (int) p2.y);
        }

        edgeIndex++;
        if (edgeIndex < edges.size()) {
            furthestEdgeDistance = edges.get(edgeIndex).getDistanceToTarget();
        } else {
            furthestEdgeDistance = -Double.MAX_VALUE;
        }
    }

    private void renderNextTriangle() {
        PlaneTriangle triangle = triangles.get(triangleIndex);

        Point2D p1 = triangle.getP1().projectionToCameraPlane;
        Point2D p2 = triangle.getP2().projectionToCameraPlane;
        Point2D p3 = triangle.getP3().projectionToCameraPlane;

        if (p1 != null && p2 != null && p3 != null) {
            screen.setColor(triangle.color.getRGB());
            screen.fillTriangle(new Triangle((int) p1.x, (int) p1.y, (int) p2.x, (int) p2.y, (int) p3.x, (int) p3.y));
        }

        triangleIndex++;
        if (triangleIndex < triangles.size()) {
            furthestTriangleDistance = triangles.get(triangleIndex).getDistanceToTarget();
        } else {
            furthestTriangleDistance = -Double.MAX_VALUE;
        }
    }
}
