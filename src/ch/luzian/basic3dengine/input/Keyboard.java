/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.luzian.basic3dengine.input;

import ch.luzian.basic3dengine.controlInterfaces.Controller;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author Luzian Bieri
 */
public class Keyboard implements KeyListener, Controller{

    private final boolean[] keys = new boolean[255];
    public boolean up, down, left, right, front, back;

    @Override
    public void init() {
    }
       
    @Override
    public void update(){
        front = keys[KeyEvent.VK_W];
        left = keys[KeyEvent.VK_A];
        back = keys[KeyEvent.VK_S];
        right = keys[KeyEvent.VK_D]; 
        up = keys[KeyEvent.VK_SPACE];
        down = keys[KeyEvent.VK_SHIFT];
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
    } 
}
