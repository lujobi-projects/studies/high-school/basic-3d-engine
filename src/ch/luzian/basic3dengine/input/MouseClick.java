/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.luzian.basic3dengine.input;

import ch.luzian.basic3dengine.controlInterfaces.Controller;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author Luzian Bieri
 */
public class MouseClick implements MouseListener, Controller{

    private static int mouseB = -1;
    
    
    @Override
    public void init() {
    }

    @Override
    public void update() {
    }
    
    public static int getButton(){
        return mouseB;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mouseB = e.getButton();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        mouseB = -1;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }
}
