/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.input;

import ch.luzian.basic3dengine.controlInterfaces.Controller;
import java.awt.AWTException;
import java.awt.Canvas;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class MouseMovement implements MouseMotionListener, Controller {

    private Robot robot = null;
    private int mouseX = 0;
    private int mouseY = 0;
    private final int midx, midy;
    private int diffX = 0, diffY = 0, zeroX, zeroY;
    private final JFrame frame;
    private final Canvas canvas;

    public MouseMovement(int midx, int midy, JFrame frame, Canvas canvas) {
        this.midx = midx;
        this.midy = midy;
        this.frame = frame;
        this.canvas = canvas;

    }

    @Override
    public void init() {
        try {
            robot = new Robot();
            robot.mouseMove(midx, midy);
        } catch (AWTException ex) {
        }
        BufferedImage cursorImg = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);

        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
        frame.getContentPane().setCursor(blankCursor);
        zeroX = frame.getX() + midx;
        zeroY = frame.getY() + midy;
    }

    boolean first = true;

    @Override
    public void update() {
        robot.mouseMove(zeroX, zeroY);
        if (!first) {
            diffX = mouseX - zeroX + frame.getX() + canvas.getX() + 3;
            diffY = mouseY - zeroY + frame.getY() + canvas.getY() + 26;
        }
        first = false;
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
    }

    public int getDiffX() {
        return diffX;
    }

    public int getDiffY() {
        return diffY;
    }
}
