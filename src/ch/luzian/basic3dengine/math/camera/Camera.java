/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.camera;

import ch.luzian.basic3dengine.controlInterfaces.Controller;
import ch.luzian.basic3dengine.Math.vectorMath.Vector3DMath;
import ch.luzian.basic3dengine.input.MouseMovement;
import ch.luzian.basic3dengine.Math.planeMath.points.Point3D;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector2D;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector3D;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector4D;
import ch.luzian.basic3dengine.input.Keyboard;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Camera implements Controller {

    public final int WIDTH, HEIGHT;
    public Vector3D rotation;
    public Point3D position;
    public double fieldOfView;
    private final double clippingNear, clippingFar;
    private double focusDistance, aspect;
    private CameraMatrix cameraMatrix;
    private final MouseMovement mouse;
    private final Keyboard keyboard;

    private final Vector3DMath vector3DMath = Vector3DMath.VECTOR3DMATH;
    private Vector3D viewingDirection;

    public Camera(int width, int height, double fieldOfView, Point3D position, Vector3D rotation, double clippingNear, double clippingFar, MouseMovement mouse, Keyboard keyboard) {
        this.WIDTH = width;
        this.HEIGHT = height;
        this.fieldOfView = fieldOfView;
        this.position = position;
        this.rotation = rotation;
        this.clippingNear = clippingNear;
        this.clippingFar = clippingFar;
        this.mouse = mouse;
        this.keyboard = keyboard;
    }

    @Override
    public void init() {
        focusDistance = Math.sin(fieldOfView / 2) / Math.cos(fieldOfView / 2);
        aspect = WIDTH / HEIGHT;
        cameraMatrix = new CameraMatrix(focusDistance, aspect, clippingNear, clippingFar);

        updateViewingDirection();
    }

    @Override
    public void update() { 
        rotation.addVector(handleRotation());
        updateViewingDirection();
        position.addVector(handleMovement());
        
    }

    public Vector2D inputVector(Point3D p) {
        if(vector3DMath.getAngleBetween(viewingDirection, new Vector3D(p.x-position.x, p.y-position.y, p.z-position.z)) > fieldOfView / 7*4){
            return null;
        }
        
        Point3D point = p.getAddedVector(position.getPositionVector3D().getInvertedVector()); // shiftVector in cameraCoordinateSystem
        point.rotate(rotation.getInvertedVector());
        
        Vector4D vectorFromCameraMatrix = cameraMatrix.inputVector(point.getPositionVector3D());
        if (vectorFromCameraMatrix.t == 0) {
            return null;
        } else if (vectorFromCameraMatrix.z < -vectorFromCameraMatrix.t || vectorFromCameraMatrix.z > vectorFromCameraMatrix.t) {
            return null;
        } else {
            return new Vector2D(vectorFromCameraMatrix.x / vectorFromCameraMatrix.t, vectorFromCameraMatrix.y / vectorFromCameraMatrix.t);
        }
    }

    //!!!!!!DOLLYZOOM
    public CameraMatrix getCameraMatrix() {
        return cameraMatrix;
    }
    
    private void updateViewingDirection(){
        Point3D pointInFront = new Point3D(0, 0, -20);
        pointInFront.rotate(rotation);
        pointInFront.addVector(position.getPositionVector3D());
        viewingDirection = new Vector3D(pointInFront.x-position.x, pointInFront.y-position.y, pointInFront.z-position.z);
    }
    private Vector3D handleRotation(){       
        Vector3D turning = new Vector3D(0, 0, 0);
        turning.x = -mouse.getDiffY()/1000.0;
        turning.y = -mouse.getDiffX() / 1000.0;
        return turning;
    }
    private Vector3D handleMovement(){
        Vector3D movement = new Vector3D(0, 0, 0);
        
        if (keyboard.back) {
            movement.z += 0.1;
        }
        if (keyboard.front) {
            movement.z -= 0.1;
        }
        if (keyboard.right) {
            movement.x += 0.1;
        }
        if (keyboard.left) {
            movement.x -= 0.1;
        }
        
        Point3D p = new Point3D(movement);
        Vector3D vRot = new Vector3D(0, 0, rotation.z);
        p.rotate(vRot);
        
        movement = new Vector3D(0, 0, 0);
        if (keyboard.up) {
            movement.y += 0.1;
        }
        if (keyboard.down) {
            movement.y -= 0.1;
        }
        
        return p.getPositionVector3D().getAddedVector(movement);
    }

    @Override
    public String toString() {
        return "Camera FOVy: " + fieldOfView + " width: " + WIDTH + " height: " + HEIGHT;
    }

}
