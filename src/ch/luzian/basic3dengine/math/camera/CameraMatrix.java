/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.camera;

import ch.luzian.basic3dengine.Math.matrices.Matrix4x4;
import ch.luzian.basic3dengine.Math.matrices.NotMatrixLengthError;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector3D;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector4D;


/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class CameraMatrix {

    private double[] cameraMatrix = new double[16];
    private Matrix4x4 matrix;
    private double z_near, z_far, focusLength, alpha, beta, aspect;
    
    public CameraMatrix(double focusLength, double aspect, double z_near, double z_far){
        for(int i = 0; i < cameraMatrix.length; i++){
            cameraMatrix[i] = 0;
        }
        cameraMatrix[14] = -1;
        try {
            matrix = new Matrix4x4(cameraMatrix);
        } catch (NotMatrixLengthError ex) {
        }
        
        setFocusLength(focusLength);
        setClipping(z_near, z_far);
        setAspect(aspect);
    }
    
    public void setAspect(double aspect){
        this.aspect = aspect;
        cameraMatrix[0] = focusLength/aspect;
    }
    public void setFocusLength(double focusLength){
        this.focusLength = focusLength;
        cameraMatrix[0] = focusLength/aspect;
        cameraMatrix[5] = focusLength;
        //cameraMatrix[10] = focusLength;
    }
    
    public void setClipping(double z_near, double z_far){
        this.z_near = z_near;
        this.z_far = z_far;
        alpha = (z_far + z_near) / (z_near-z_far);
        beta = (2* z_far * z_near)/(z_near-z_far);
        cameraMatrix[10] = alpha;
        cameraMatrix[11] = beta;
    }
    
    public void setNearPlane(double z_near){
        setClipping(z_near, this.z_far);
    }
    
    public void setFarPlane(double z_far){
        setClipping(this.z_near, z_far);
    }
    
    
    public Vector4D inputVector(Vector3D v){
        Vector4D vec = new Vector4D(v, 1);
        Vector4D vector = matrix.multiplyWith4DVector(vec);
        return vector;
    }
    
    @Override
    public String toString(){
        return "CameraMatrix FocusLength: " + focusLength + " Aspect: " + aspect;
    }
}
