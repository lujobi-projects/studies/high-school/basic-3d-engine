/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.coordinatesystem;

import ch.luzian.basic3dengine.Engine;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class CoordinateSystem2D {

    private double x_min, x_max, x_diff;
    private double y_min, y_max, y_diff;

    private int screenWidth, screenHeight;

    public CoordinateSystem2D(int width, int height) {
        this(width, height, -1/Engine.RATIO, 1/Engine.RATIO, -1, 1);
        //this(width, height, -1, 1, -1, 1);
    }

    public CoordinateSystem2D(int width, int height, double x_min, double x_max,
            double y_min, double y_max) {
        this.screenWidth = width;
        this.screenHeight = height;

        setCoordinateSystem(x_min, x_max, y_min, y_max);
    }

    public void setCoordinateSystem(double x_min, double x_max, double y_min, double y_max) {
        this.x_min = x_min;
        this.x_max = x_max;
        this.y_min = y_min;
        this.y_max = y_max;

        x_diff = Math.abs(x_max - x_min);
        y_diff = Math.abs(y_max - y_min);
    }

    public int getPixelX(double x_user) {
        double x_java = Math.abs(x_user - x_min) * screenWidth / x_diff;
        return (int) (x_java*Math.signum(x_user-x_min));
    }

    public int getPixelY(double y_user) {
        double y_java = Math.abs(y_user - y_max) * screenHeight / y_diff;
        return (int) (y_java*Math.signum(y_max - y_user));
    }

    public double getCoordX(int x_java) {
        double x_user = x_java * x_diff / screenWidth;
        return (x_min < x_max) ? x_min + x_user : x_min - x_user;
    }

    public double getCoordY(int y_java) {
        double y_user = y_java * y_diff / screenHeight;
        return (y_min < y_max) ? y_max - y_user : y_max + y_user;
    }

    public double getStepX() {
        return x_diff / screenWidth;
    }

    public double getStepY() {
        return y_diff / screenHeight;
    }
}
