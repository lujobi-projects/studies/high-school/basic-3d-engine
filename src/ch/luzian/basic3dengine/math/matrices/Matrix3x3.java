/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.matrices;

import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector3D;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Matrix3x3 {

    private final double[] matrix;
    
    public Matrix3x3(double[] matrix){
        this.matrix = matrix;
    }
    
    public Vector3D multiplyWith3DVector(Vector3D v){
        double x = (matrix[0] * v.x) + (matrix[1]*v.y) + (matrix[2] * v.z);
        double y = (matrix[3] * v.x) + (matrix[4]*v.y) + (matrix[5] * v.z);
        double z = (matrix[6] * v.x) + (matrix[7]*v.y) + (matrix[9] * v.z);
        Vector3D vec = new Vector3D(x, y, z);
        return vec;
    }
    
    public void set(int index, double value){
        matrix[index] = value;
    }
}
