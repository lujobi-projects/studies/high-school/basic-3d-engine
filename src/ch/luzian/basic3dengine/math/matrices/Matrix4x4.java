/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.matrices;

import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector4D;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Matrix4x4 {

    private final double[] matrix;
    
    public Matrix4x4(double[] matrix) throws NotMatrixLengthError {
        this.matrix = matrix;
        if(matrix.length != 16){
            throw new NotMatrixLengthError(matrix.length, 16);
        }
    }
    
    public Vector4D multiplyWith4DVector(Vector4D v){
        double x = (matrix[0] * v.x) + (matrix[1]*v.y) + (matrix[2] * v.z) + (matrix[3]*v.t);
        double y = (matrix[4] * v.x) + (matrix[5]*v.y) + (matrix[6] * v.z) + (matrix[7]*v.t);
        double z = (matrix[8] * v.x) + (matrix[9]*v.y) + (matrix[10] * v.z) + (matrix[11]*v.t);
        double t = (matrix[12] * v.x) + (matrix[13]*v.y) + (matrix[14] * v.z) + (matrix[15]*v.t);
        Vector4D vec = new Vector4D(x, y, z, t);
        return vec;
    }
    
    public void set(int index, double value){
        matrix[index] = value;
    }
}
