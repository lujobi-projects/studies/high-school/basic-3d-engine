/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.planeMath.lines;

import ch.luzian.basic3dengine.Math.planeMath.points.Point3D;
import java.awt.Color;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Edge implements Comparable<Edge> {

    private final Point3D p1, p2;
    private final Point3D middlePoint;
    private double distanceToTarget = 0;
    public Color color = Color.blue;

    public Edge(Point3D p1, Point3D p2) {
        this.p1 = p1;
        this.p2 = p2;
        middlePoint = new Point3D(0, 0, 0, p1.cameraPosition);
    }

    public void update() {
        middlePoint.x = (p1.x + p2.x) / 2;
        middlePoint.y = (p1.y + p2.y) / 2;
        middlePoint.z = (p1.z + p2.z) / 2;

        middlePoint.update();

        distanceToTarget = Math.max(Math.max(middlePoint.getDistanceToTarget(), p1.getDistanceToTarget()),p2.getDistanceToTarget());
    }

    public boolean equals(Edge e) {
        return (p1.equals(e.p1) && p2.equals(e.p2)) || (p2.equals(e.p1) && p1.equals(e.p2));
    }

    public double getDistanceToTarget() {
        return distanceToTarget;
    }

    public Point3D getP1() {
        return p1;
    }

    public Point3D getP2() {
        return p2;
    }

    @Override
    public int compareTo(Edge e) {
        return Double.compare(e.getDistanceToTarget(), distanceToTarget);
    }
}
