/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.planeMath.lines;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Line {

    private final int x1, x2, y1, y2;
    private final double m, q;

    public Line(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
        if (x2 != x1) {
            m = (1.0 * y2 - 1.0 * y1) / (1.0 * x2 - 1.0 * x1);
        } else {
            m = 1000000000;
        }
        q = y1 - (m * x1);
    }

    public int findBestXtoY(int y) {
        if ((y1 <= y2 && y >= y1 && y <= y2) || (y1 >= y2 && y >= y2 && y <= y1)) {
            return functionOfY(y);
        }else{
            return -1;
        }
    }
    
    public int findBestYtoX(int x) {
        if ((x1 <= x2 && x >= x1 && x <= x2) || (x1 >= x2 && x >= x2 && x <= x1)) {
            return functionOfX(x);
        }else{
            return -1;
        }
    }

    private int functionOfY(int y) {
        if (m != 0) {
            return Math.round((float)((y-q)/m));
        }else{
            return -1;
        }
    }
    
    private int functionOfX(int x) {
        if (m != 1000000000) {
            return Math.round((float)((m*x)+q));
        }else{
            return -1;
        }
    }
}
