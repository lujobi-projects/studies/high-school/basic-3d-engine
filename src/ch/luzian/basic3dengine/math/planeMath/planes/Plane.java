/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.planeMath.planes;

import ch.luzian.basic3dengine.Math.planeMath.points.Point3D;
import ch.luzian.basic3dengine.Math.vectorMath.Vector3DMath;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector3D;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Plane {

    private double  d;
    private Vector3D normalVector;
    private final Vector3D p1, p2, p3;
    
    public Plane(Point3D p1, Point3D p2, Point3D p3){
        this.p1 = p1.getPositionVector3D();
        this.p2 = p2.getPositionVector3D();
        this.p3 = p3.getPositionVector3D();
        
        update();
    }
    
    public void update(){
        normalVector = Vector3DMath.VECTOR3DMATH.getCrossProduct(p1.getAddedVector(p3.getInvertedVector()), p2.getAddedVector(p3.getInvertedVector()));
        d = -((normalVector.x*p1.x)+ (normalVector.y*p1.y)+(normalVector.z*p1.z));
    }
    
    public Vector3D getNormalVector(){
        return normalVector;
    }

    @Override
    public String toString() {
        return "Plane with normalVector = " + normalVector + " and d = " + d + "  ";
    }
}
