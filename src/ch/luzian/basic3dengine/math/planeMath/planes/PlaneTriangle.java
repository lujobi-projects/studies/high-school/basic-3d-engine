/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.planeMath.planes;

import ch.luzian.basic3dengine.Math.planeMath.points.Point3D;
import ch.luzian.basic3dengine.Math.planeMath.lines.Edge;
import java.awt.Color;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class PlaneTriangle implements Comparable<PlaneTriangle>{
    
    public Point3D p1, p2, p3;
    public Edge e1, e2, e3;
    private final Plane plane;
    
    private final Point3D middlePoint;
    public double distanceToTarget = 0;
    public Color color = Color.white;

    public PlaneTriangle(Point3D p1, Point3D p2, Point3D p3, Edge e1, Edge e2, Edge e3){
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.e1 = e1;
        this.e2 = e2;
        this.e3 = e3;
        
        middlePoint = new Point3D(0, 0, 0, p1.cameraPosition);
        plane = new Plane(p1, p2, p3);
    }
    
    public void update(){
        middlePoint.x = (p1.x + p2.x + p3.x)/3;
        middlePoint.y = (p1.y + p2.y + p3.y)/3;
        middlePoint.z = (p1.z + p2.z + p3.z)/3;
        
        middlePoint.update(); 
        distanceToTarget = Math.max(Math.max(middlePoint.getDistanceToTarget(), p1.getDistanceToTarget()), Math.max(p2.getDistanceToTarget(), p3.getDistanceToTarget()));
        plane.update();
    }

    public double getDistanceToTarget() {
        return distanceToTarget;
    }
    public Point3D getP1() {
        return p1;
    }
    public Point3D getP2() {
        return p2;
    }
    public Point3D getP3() {
        return p3;
    }
    

    @Override
    public int compareTo(PlaneTriangle o) {
        return Double.compare(o.getDistanceToTarget(), distanceToTarget);
    }
    
}
