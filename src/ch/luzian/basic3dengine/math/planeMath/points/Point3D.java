/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.planeMath.points;

import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector3D;
import java.awt.Color;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Point3D implements Comparable<Point3D> {

    public double x, y, z;
    public Color color = Color.red;
    private double distanceToTarget = 0;
    public Point3D cameraPosition = null;
    public Point2D projectionToCameraPlane = null;
    private final Vector3D positionVector = new Vector3D(0, 0, 0);
    private final PointMath math = PointMath.POINTMATH;

    public Point3D(double x, double y, double z, Point3D cameraPosition) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.cameraPosition = cameraPosition;

        updatePoint();
        update();
    }

    public Point3D(Vector3D v, Point3D cameraPosition) {
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
        this.cameraPosition = cameraPosition;

        updatePoint();
        update();
    }

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.cameraPosition = null;

        updatePoint();
        update();
    }

    public Point3D(Vector3D v) {
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
        this.cameraPosition = null;

        updatePoint();
        update();
    }

    public void setToPoint(Point3D p) {
        x = p.x;
        y = p.y;
        z = p.z;
        updatePoint();
    }

    public void setToVector(Vector3D v) {
        x = v.x;
        y = v.y;
        z = v.z;
        updatePoint();
    }

    public void addVector(Vector3D v) {
        x += v.x;
        y += v.y;
        z += v.z;
        updatePoint();
    }

    public Point3D getAddedVector(Vector3D v) {
        return new Point3D(x + v.x, y + v.y, z + v.z);
    }

    public void update() {
        if (cameraPosition != null) {
            distanceToTarget = Math.sqrt(Math.pow(x - cameraPosition.x, 2) + Math.pow(y - cameraPosition.y, 2) + Math.pow(z - cameraPosition.z, 2));
        }
    }

    private void updatePoint() {
        positionVector.x = x;
        positionVector.y = y;
        positionVector.z = z;
    }

    public double getDistanceToTarget() {
        return distanceToTarget;
    }

    public Vector3D getPositionVector3D() {
        return positionVector;
    }

    public void rotateX(Point3D center, double angle) {
        math.rotateXAxis(this, center, angle);
    }

    public void rotateY(Point3D center, double angle) {
        math.rotateYAxis(this, center, angle);
    }

    public void rotateZ(Point3D center, double angle) {
        math.rotateZAxis(this, center, angle);
    }

    public void rotateX(double angle) {
        math.rotateXAxis(this, angle);
    }

    public void rotateY(double angle) {
        math.rotateYAxis(this, angle);
    }

    public void rotateZ(double angle) {
        math.rotateZAxis(this, angle);
    }
    
    public void rotate(Vector3D rotation){
        if (rotation.x != 0) {
            rotateX(rotation.x);
        }
        if (rotation.y != 0) {
            rotateY(rotation.y);
        }
        if (rotation.z != 0) {
            rotateZ(rotation.z);
        }
    }
    
    public void rotate(Point3D center, Vector3D rotation){
        if (rotation.x != 0) {
            rotateX(center, rotation.x);
        }
        if (rotation.y != 0) {
            rotateY(center, rotation.y);
        }
        if (rotation.z != 0) {
            rotateZ(center, rotation.z);
        }
    }

    public boolean equals(Point3D p) {
        return x == p.x && y == p.y && z == p.z;
    }

    @Override
    public String toString() {
        return "Point3D: [ " + x + " / " + y + " / " + z + " ]";
    }

    @Override
    public int compareTo(Point3D p) {
        return Double.compare(p.getDistanceToTarget(), distanceToTarget);
    }
}
