/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.planeMath.points;

import ch.luzian.basic3dengine.Math.matrices.Matrix4x4;
import ch.luzian.basic3dengine.Math.matrices.NotMatrixLengthError;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector4D;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class PointMath {
    
    public static PointMath POINTMATH = new PointMath();
    
    private final double[] rotationMatrixList = new double[16];
    private Matrix4x4 rotationMatrix;

    public PointMath() {
                
        resetRotationMatrix();
        try {
            this.rotationMatrix = new Matrix4x4(rotationMatrixList);
        } catch (NotMatrixLengthError ex) {
        }
    }
    
    public void rotateXAxis(Point3D p, Point3D center, double angle){
        p.addVector(center.getPositionVector3D().getInvertedVector());
        rotateXAxis(p, angle);
        p.addVector(center.getPositionVector3D());
    }    
    public void rotateYAxis(Point3D p, Point3D center, double angle){
        p.addVector(center.getPositionVector3D().getInvertedVector());
        rotateYAxis(p, angle);
        p.addVector(center.getPositionVector3D());
    }
    public void rotateZAxis(Point3D p, Point3D center, double angle){
        p.addVector(center.getPositionVector3D().getInvertedVector());
        rotateZAxis(p, angle);
        p.addVector(center.getPositionVector3D());
    }
        
    public void rotateXAxis(Point3D p, double angle){
        rotationMatrixList[0] = 1;
        rotationMatrixList[5] = Math.cos(angle);
        rotationMatrixList[6] = -Math.sin(angle);
        rotationMatrixList[9] = Math.sin(angle);
        rotationMatrixList[10] = Math.cos(angle);
        rotationMatrixList[15] = 1;
        rotate(p);
        resetRotationMatrix();
    }
    public void rotateYAxis(Point3D p, double angle){
        rotationMatrixList[0] = Math.cos(angle);
        rotationMatrixList[2] = Math.sin(angle);
        rotationMatrixList[5] = 1;
        rotationMatrixList[8] = -Math.sin(angle);
        rotationMatrixList[10] = Math.cos(angle);
        rotationMatrixList[15] = 1;
        rotate(p);
        resetRotationMatrix();
    }
    public void rotateZAxis(Point3D p, double angle){
        rotationMatrixList[0] = Math.cos(angle);
        rotationMatrixList[1] = -Math.sin(angle);
        rotationMatrixList[4] = Math.sin(angle);
        rotationMatrixList[5] = Math.cos(angle);
        rotationMatrixList[10] = 1;
        rotationMatrixList[15] = 1;
        rotate(p);
        resetRotationMatrix();
    }
    
    private void rotate(Point3D p){
        p.setToVector(rotationMatrix.multiplyWith4DVector(new Vector4D(p.getPositionVector3D(), 1)).get3DPart());
    }
    private void resetRotationMatrix(){
        for(int i = 0; i < rotationMatrixList.length; i++){
            rotationMatrixList[i] = 0;
        }
    }
}
