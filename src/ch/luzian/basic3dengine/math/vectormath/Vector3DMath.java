/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.vectorMath;

import ch.luzian.basic3dengine.Math.matrices.Matrix4x4;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector3D;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */

public class Vector3DMath {
    public static final Vector3DMath VECTOR3DMATH= new Vector3DMath();
    
    private final double[] rotationMatrixList = new double[16];
    private Matrix4x4 rotationMatrix;

    public Vector3DMath() {
    }
    
    public double getAngleBetween(Vector3D v1, Vector3D v2){
        return Math.acos(getDotProduct(v1, v2)/v1.getLength()/v2.getLength());
    }
    
    public double getDotProduct(Vector3D v1, Vector3D v2){
        return (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z);
    }
    
    public Vector3D getCrossProduct(Vector3D v1, Vector3D v2){
        return new Vector3D(((v1.y*v2.z)-(v1.z*v2.y)), ((v1.z*v2.x)-(v1.x*v2.z)), ((v1.x*v2.y)-(v1.y*v2.x)));
    }
}
