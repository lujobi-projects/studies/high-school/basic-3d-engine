/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.vectorMath.vectors;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Vector2D {

    public double x, y;
    
    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }
        
    public double getLength(){
        return Math.sqrt((x*x)+(y*y));
    }   
    public void normalize(){
        scale(1/getLength());
    }    
    public Vector2D getNormalized(){
        return getScaledVector(1/getLength());
    }   
    public void scale(double scalingFactor){
        x *= scalingFactor;
        y *= scalingFactor;
    }
    public Vector2D getScaledVector(double scalingFactor){
        return new Vector2D(x * scalingFactor, y * scalingFactor);
    }
    public void addVector(Vector2D vector){
        x = x + vector.x;
        y = y + vector.y;
    }
    public Vector2D getAddedVector(Vector2D vector){
        return new Vector2D(x + vector.x, y + vector.y);
    }   
    public void multVector(Vector2D vector){
        x = x * vector.x;
        y = y * vector.y;
    }   
    public Vector2D getMultipliedVector(Vector2D vector){
        return new Vector2D(x * vector.x, y * vector.y);
    }
    
    public Vector3D get3DVector(){
        return new Vector3D(x, y, 0);
    }
    
    @Override
    public String toString(){
        return "Vector2D: [ " +x + " / " + y + " ]";
    }
}
