/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.vectorMath.vectors;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Vector3D {

    public double x, y, z;
    
    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
        
    public double getLength(){
        return Math.sqrt((x*x)+(y*y)+(z*z));
    }   
    public void normalize(){
        scale(1/getLength());
    }    
    public Vector3D getNormalized(){
        return getScaledVector(1/getLength());
    }   
    public void scale(double scalingFactor){
        x *= scalingFactor;
        y *= scalingFactor;
        z *= scalingFactor;
    }
    public Vector3D getScaledVector(double scalingFactor){
        return new Vector3D(x * scalingFactor, y * scalingFactor, z*scalingFactor);
    }
    public void addVector(Vector3D vector){
        x = x + vector.x;
        y = y + vector.y;
        z = z + vector.z;
    }
    public Vector3D getAddedVector(Vector3D vector){
        return new Vector3D(x + vector.x, y + vector.y, z + vector.z);
    }   
    public Vector3D getInvertedVector(){
        return new Vector3D(-x, -y, -z);
    }
    public void multVector(Vector3D vector){
        x = x * vector.x;
        y = y * vector.y;
        z = z * vector.z;
    }   
    public Vector3D getMultipliedVector(Vector3D vector){
        return new Vector3D(x * vector.x, y * vector.y, z * vector.z);
    }
    
    @Override
    public String toString(){
        return "Vector3D: [ " +x + " / " + y + " / " + z +" ]";
    }
}
