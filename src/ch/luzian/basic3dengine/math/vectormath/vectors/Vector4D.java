/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.Math.vectorMath.vectors;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Vector4D {

    public double x, y, z, t;
    
    public Vector4D(Vector3D v, double t) {
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
        this.t = t;
    }
    
    public Vector4D(double x, double y, double z, double t) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.t = t;
    }
    
    public Vector3D get3DPart(){
        return new Vector3D(x/t, y/t, z/t);
    }
         
    @Override
    public String toString(){
        return "Vector4D: [ " +x + " / " + y + " / " + z + " / " + t +" ]";
    }
}
