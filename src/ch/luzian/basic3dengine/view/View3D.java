/*
 * Copyright (C) 2017 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.basic3dengine.view;

import ch.luzian.basic3dengine.graphics.figures.Figure3D;
import ch.luzian.basic3dengine.controlInterfaces.Controller;
import ch.luzian.basic3dengine.graphics.figures.*;
import ch.luzian.basic3dengine.Math.vectorMath.vectors.Vector3D;
import ch.luzian.basic3dengine.Math.planeMath.points.Point3D;
import ch.luzian.basic3dengine.graphics.queues.RenderQueue;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class View3D implements Controller{

    private final Point3D cameraPosition;
    private Figure3D sphere1, sphere2;
    private final RenderQueue renderQueue;

    public View3D(Point3D camera, RenderQueue renderQueue) {
        this.cameraPosition = camera;
        this.renderQueue = renderQueue;
    }
   
    @Override
    public void init() {
        sphere1 = new Sphere(new Point3D(0, 0, -20), cameraPosition, 5, 15); 
        sphere2 = new Sphere(new Point3D(4, 3, -15), cameraPosition, 5, 15); 
        sphere1.generate();
        sphere2.generate();
        
        renderQueue.addToRenderQueue(sphere1);
        renderQueue.addToRenderQueue(sphere2);
    }
    
    @Override
    public void update() {
        Vector3D movement = new Vector3D(0, 0, 0);
        Vector3D rotation = new Vector3D(0.002, 0.001, 0.003);
        
        sphere1.move(movement);
        sphere2.move(movement);
        sphere1.rotate(rotation);
        sphere2.rotate(rotation.getInvertedVector());
        sphere1.update();
        sphere2.update();
    }
}
